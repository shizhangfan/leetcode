package org.shizf.bits;

/**
 * @author shizhangfan
 * @date 2022/4/6 - 17:19
 */
public class RightShift {
    public static void main(String[] args) {
        int i = -1; // 11111111 11111111 11111111 11111111
        // 无脑补零 逻辑右移 >>>
        int j = i >>> 1; // 01111111 11111111 11111111 11111111 （
        System.out.println(j); // 2147483647

        // 带符号位右移，算数右移 >>
        int k = i >> 1; // 11111111 11111111 11111111 11111111
        System.out.println(k); // -1
    }
}
