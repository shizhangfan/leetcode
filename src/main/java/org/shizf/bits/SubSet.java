package org.shizf.bits;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shizhangfan
 * @date 2022/4/2 - 15:50
 */
public class SubSet {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        if (nums.length == 0) {
            return res;
        }
        for(int i = 0; i < 1 << nums.length; i++) {
            List<Integer> tmp = new ArrayList<>();
            for (int j = 0; j < nums.length; j++) {
                if (((i >> j) & 1) == 1) {
                    tmp.add(nums[j]);
                }
            }
            res.add(tmp);
        }
        return res;
    }

    public static void main(String[] args) {
        SubSet subSet = new SubSet();
        List<List<Integer>> subsets = subSet.subsets(new int[]{1, 2, 3});
        for (List<Integer> subset : subsets) {
            System.out.print("{");
            String collect = subset.stream().map(Object::toString).collect(Collectors.joining(","));
            System.out.print(collect);
            System.out.println("}");
        }
    }
}
