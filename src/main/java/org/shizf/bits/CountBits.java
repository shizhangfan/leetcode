package org.shizf.bits;

/**
 * @author shizhangfan
 * @date 2022/4/6 - 17:49
 */
public class CountBits {
    public static void main(String[] args) {
        int n = 23;

        int res = 0;
        while (n > 0) {
            if ((n & 1) != 0) res += 1;
            n = n >> 1;
        }
        System.out.println(res);
    }
}
