package org.shizf.bits;

/**
 * @author shizhangfan
 * @date 2022/4/6 - 17:32
 */
public class IsIthSet {
    public static void main(String[] args) {
        int n = 12;//1100
        int j = 1;// 0001

        int k = j << 2;

        int res = k & n; // 0100
        if (res == 0) {
            System.out.println("False");
        } else {
            System.out.println("True");
        }

        int k1 = j << 1;

        int res1 = k1 & n; // 0010
        if (res1 == 0) { // False
            System.out.println("False");
        } else {
            System.out.println("True");
        }
    }
}
