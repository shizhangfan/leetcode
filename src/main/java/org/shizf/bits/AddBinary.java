package org.shizf.bits;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author shizhangfan
 * @date 2022/4/2 - 18:41
 */
public class AddBinary {
    public String addBinary(String a, String b) {
        int i = a.length();
        int j = b.length();
        int carry = 0;
        Deque<Integer> stack = new LinkedList<>();
        for(int k = Math.min(i, j) - 1; k >= 0; k--) {
            char c1 = a.charAt(k);
            char c2 = b.charAt(k);
            int sum = Integer.parseInt(String.valueOf(c1)) + Integer.parseInt(String.valueOf(c2)) + carry;
            carry = sum > 1 ? 1 : 0;
            int n = sum % 2;
            stack.offer(n);
        }

        int l = Math.abs(i - j) - 1;
        String tmp = i > j ? a : b;
        while(l >= 0) {
            char c = tmp.charAt(l);
            int sum = Integer.parseInt(String.valueOf(c)) + carry;
            carry = sum > 1 ? 1 : 0;
            stack.offer(sum % 2);
            l--;
        }
        if (carry > 0) {
            stack.offer(1);
        }

        StringBuilder res = new StringBuilder();
        while(!stack.isEmpty()) {
            res.append(stack.remove().toString());
        }

        return res.toString();
    }
    public static void main(String[] args) {
        AddBinary a = new AddBinary();
        String s = a.addBinary("11", "1");
        System.out.println(s);
    }
}
