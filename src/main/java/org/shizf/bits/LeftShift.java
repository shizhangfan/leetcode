package org.shizf.bits;

/**
 * @author shizhangfan
 * @date 2022/4/6 - 17:12
 */
public class LeftShift {
    public static void main(String[] args) {
        int i = -1;
        i = i << 2;
        System.out.println(i);

        int j = -1073741825; // 10111111 11111111 11111111 11111111
        j = j << 1;          // 01111111 11111111 11111111 11111111  (2147483646)
        System.out.println(j);

        int k = -2147483647; // 10000000 00000000 00000000 00000001
        k = k << 1;          // 00000000 00000000 00000000 00000010  (2)
        System.out.println(k);
    }
}
