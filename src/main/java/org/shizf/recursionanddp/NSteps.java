package org.shizf.recursionanddp;

public class NSteps {
    public static void main(String[] args) {
        NSteps nSteps = new NSteps();
        int n = nSteps.nSteps(4);
        System.out.println(n);
    }

    public int nSteps(int n) {
        if (n < 3) {
            return n;
        }

        if (n == 3) {
            return 4;
        }

        int[] dp = new int[n + 1];
        dp[0] = 0;
        dp[1] = 1;
        dp[2] = dp[1] + 1;
        dp[3] = dp[1] + dp[2] + 1;

        for (int i = 4; i < n + 1; i++) {
            dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3];
        }

        return dp[n];
    }
}
