package org.shizf.twopointers;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shizhangfan
 * @date 2022/4/14 - 9:48
 */
public class NumbersSum {
    public static void main(String[] args) {
        int[] nums = new int[] {1, 2, 3, 4, 5};
        int target = 10;
        NumbersSum sum = new NumbersSum();
        boolean b = sum.twoSumWithMap(nums, target);
        System.out.println(b);
    }
    /**
     * 使用 HashMap 来保存数组内的元素
     * Map<元素值，N(元素个数)>
     * 查找的时候，遍历 MAP，得到 TARGET - KEY，看 是否存在这个值，如果存在，且 N > 0，就说明是符合题目条件的
     * @param nums 数组
     * @param target 目标数
     * @return 是否存在
     */
    public boolean twoSumWithMap(int[] nums, int target) {
        if (nums.length == 0) {return false;}

        Map<Integer, Integer> map = new HashMap<>(nums.length);
        for (int num : nums) {
            if (map.containsKey(num)) {
                Integer val = map.get(num);
                map.put(num, ++val);
            } else {
                map.put(num, 1);
            }
        }

        for (Integer key : map.keySet()) {
            int i = target - key;
            if (map.containsKey(i)) {
                if (i != key) {
                    return true;
                } else {
                    if(map.get(i) > 1) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
