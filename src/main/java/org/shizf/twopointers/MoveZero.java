package org.shizf.twopointers;

import java.util.Arrays;

/**
 * @author shizhangfan
 * @date 2022/4/12 - 18:41
 */
public class MoveZero {
    public static void main(String[] args) {
        int[] nums = new int[] {1, 2, 0, 5, 0, 6, 0, 3, 8, 0, 2, 0, 3, 0, 9, 0, 0, 3, 87};
        MoveZero moveZero = new MoveZero();
        moveZero.moveZeroes(nums);
        System.out.println(Arrays.toString(nums));
    }

    public void moveZeroes(int[] nums) {
        if (nums.length <= 1) return;

        int slow = 0;
        int fast = 0;

        while(slow < nums.length && nums[slow] != 0) {
            slow++;
            fast++;
        }

        while (fast < nums.length) {
            while (fast < nums.length && nums[fast] == 0) {
                fast++;
            }

            if (fast < nums.length) {
                nums[slow] = nums[slow] + nums[fast];
                nums[fast] = nums[slow] - nums[fast];
                nums[slow] = nums[slow] - nums[fast];
                slow++;
            }
        }
    }
}
