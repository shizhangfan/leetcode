package org.shizf.twopointers;

/**
 * @author shizhangfan
 * @date 2022/4/12 - 10:29
 */
public class StringPalindrome {
    public static void main(String[] args) {
        StringPalindrome stringPalindrome = new StringPalindrome();
        boolean palindrome = stringPalindrome.isPalindrome("A man, a plan, a canal: Panama");
        System.out.println(palindrome);
    }

    public boolean isPalindrome(String s) {
        if (s.length() <= 1) return true;

        int l = 0;
        int r = s.length() -1;

        while(l < r) {
            char cl = s.charAt(l);
            char cr = s.charAt(r);

            if (!Character.isLetterOrDigit(cl)) {
                l++;
                continue;
            }

            if (!Character.isLetterOrDigit(cr)) {
                r--;
                continue;
            }

            if (Character.toLowerCase(cl) == Character.toLowerCase(cr)) {
                l++;
                r--;
            } else {
                return false;
            }
        }
        return true;
    }
}
