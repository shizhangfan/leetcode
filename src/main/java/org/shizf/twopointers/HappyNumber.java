package org.shizf.twopointers;

/**
 * @author shizhangfan
 * @date 2022/4/12 - 18:26
 */
public class HappyNumber {
    public static void main(String[] args) {
        HappyNumber happyNumber = new HappyNumber();
        boolean happy = happyNumber.isHappy(19);
        System.out.println(happy);
    }

    public boolean isHappy(int n) {
        int slow = n;
        int fast = getNextNum(n);
        while (slow != fast) {
            slow = getNextNum(slow);
            System.out.println("slow:" + slow);
            fast = getNextNum(fast);
            System.out.println("fast:" + fast);
            fast = getNextNum(fast);
            System.out.println("fast:" + fast);
        }

        return slow == 1;
    }

    public int getNextNum(int n) {
        int num = 0;
        while (n > 0) {
            int x = n % 10;
            num += x * x;
            n /= 10;
        }
        return num;
    }
}
